package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task1.DBManager1;
import com.epam.rd.java.basic.task2.DBManager2;
import com.epam.rd.java.basic.task3.DBManager3;
import com.epam.rd.java.basic.task4.DBManager4;
import com.epam.rd.java.basic.task5.DBManager5;
import com.epam.rd.java.basic.task6.DBManager6;
import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private DBManager1 dbManager1 = new DBManager1();
	private DBManager2 dbManager2 = new DBManager2();
	private DBManager3 dbManager3 = new DBManager3();
	private DBManager4 dbManager4 = new DBManager4();
	private DBManager5 dbManager5 = new DBManager5();
	private DBManager6 dbManager6 = new DBManager6();


	private DBManager() {
	}

	public static synchronized DBManager getInstance()  {
		if (instance == null){
		instance = new DBManager();}
		return instance;
	}

	public static Connection getConnection() {
		Properties properties = new Properties();
		Connection conn = null;
		try (InputStream stream = new FileInputStream("app.properties")){
			properties.load(stream);
			conn = DriverManager.getConnection(properties.getProperty("connection.url"));
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public List<User> findAllUsers() throws DBException {
		return dbManager1.findAllUsers();
	}

	public boolean insertUser(User user) throws DBException {
		int insertId = dbManager1.insertUser(user);
		user.setId(insertId);
		return insertId > 0;
	}

	public boolean deleteUsers(User... users) throws DBException {
		return dbManager6.deleteUsers(users);
	}

	public User getUser(String login) throws DBException {
		return dbManager3.getUser(login);
	}

	public Team getTeam(String name) throws DBException {
		return dbManager3.getTeam(name);
	}

	public List<Team> findAllTeams() throws DBException {
		return dbManager2.findAllTeams();
	}

	public boolean insertTeam(Team team) throws DBException {
		int insertId = dbManager2.insertTeam(team);
		team.setId(insertId);
		return insertId > 0;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		return dbManager3.setTeamsForUser(user, teams);
	}

	public List<Team> getUserTeams(User user) throws DBException {
		return dbManager3.getUserTeams(user);
	}

	public boolean deleteTeam(Team team) throws DBException {
		return dbManager4.deleteTeam(team);
	}

	public boolean updateTeam(Team team) throws DBException {
		return dbManager5.updateTeam(team);
	}

}
