package com.epam.rd.java.basic.task1;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.DBManager;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DBManager1 {

    private static final String INSERT_USER = "INSERT INTO users (login) VALUES(?);";
    private static final String FIND_ALL_USERS = "SELECT * FROM users;";

    public int insertUser(User user) throws DBException{
        List<User> list = findAllUsers();
        if (list != null && list.contains(user)) return 0;
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = DBManager.getConnection();
            pstmt = con.prepareStatement(INSERT_USER,
                    Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, user.getLogin());
            pstmt.executeUpdate();
           try ( ResultSet rs = pstmt.getGeneratedKeys()){
               if (rs.next()){return rs.getInt(1);}
               return 0;
           }
        } catch (SQLException e) {
            throw new DBException("Exception for insertUser", e);
        } finally {
            closeAutoCloseable(pstmt);
            closeAutoCloseable(con);
        }
    }

    public List<User> findAllUsers() throws DBException{
        List<User> result;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            con = DBManager.getConnection();
            stmt = con.prepareStatement(FIND_ALL_USERS);
            rs = stmt.executeQuery();
            result = new ArrayList<>();
            while (rs.next()){
               User user = new User(rs.getInt(1), rs.getString(2));
               result.add(user);
            }
        } catch (SQLException e) {
            throw new DBException("Exception for findUsers", e);
        }
        finally {
            closeAutoCloseable(rs);
            closeAutoCloseable(stmt);
            closeAutoCloseable(con);
        }
        return result;
    }
    private void closeAutoCloseable(AutoCloseable con) {
        try {
            if (con != null) con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
