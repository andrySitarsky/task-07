package com.epam.rd.java.basic.task6;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.DBManager;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager6 {

    private static final String DELETE_USERS = "DELETE FROM users WHERE id IN(";

    public boolean deleteUsers(User... users) throws DBException {
        Connection con = null;
        Statement pstmt = null;
        try {
            con = DBManager.getConnection();
            pstmt = con.createStatement();
            con.setAutoCommit(false);
            pstmt.executeUpdate(getSqlDeleteUser(users));
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new DBException("Exception for deleteUsers", e);
        } finally {
            closeAutoCloseable(pstmt);
            closeAutoCloseable(con);
        }
        return true;
    }

    private String getSqlDeleteUser(User[] users) {
        StringBuilder sql = new StringBuilder().append(DELETE_USERS);
        for (User user : users) {
            sql.append(user.getId()).append(",");
        }
        int lastSeparator = sql.lastIndexOf(",");
        sql.replace(lastSeparator,lastSeparator+1,");");
        return sql.toString();
    }

    private void closeAutoCloseable(AutoCloseable con) {
        try {
            if (con != null) con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void rollback(Connection con) {
        try {
            con.rollback();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
