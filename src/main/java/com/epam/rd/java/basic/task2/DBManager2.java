package com.epam.rd.java.basic.task2;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.DBManager;
import com.epam.rd.java.basic.task7.db.entity.Team;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBManager2 {

    private static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES(?);";
    private static final String FIND_ALL_TEAMS = "SELECT * FROM teams;";

    public int insertTeam(Team team) throws DBException {
        List<Team> list = findAllTeams();
        if (list != null && list.contains(team)) return 0;
        try(Connection con = DBManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(INSERT_TEAM,
                    Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, team.getName());
            pstmt.executeUpdate();
            try ( ResultSet rs = pstmt.getGeneratedKeys()){
                if (rs.next()){return rs.getInt(1);}
                return 0;
            }
        } catch (SQLException e) {
            throw new DBException("Exception for insertTeam", e);
        }
    }

    public List<Team> findAllTeams() throws DBException{
        List<Team> result;
        try(Connection con = DBManager.getConnection();
            PreparedStatement stmt = con.prepareStatement(FIND_ALL_TEAMS);
            ResultSet rs = stmt.executeQuery()
        ){
            result = new ArrayList<>();
            while (rs.next()){
                Team team = new Team(rs.getInt(1), rs.getString(2));
                result.add(team);
            }
        } catch (SQLException e) {
            throw new DBException("Exception for findAllTeam", e);
        }
        return result;
    }
}
