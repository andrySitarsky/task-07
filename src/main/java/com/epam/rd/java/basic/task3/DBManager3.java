package com.epam.rd.java.basic.task3;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.DBManager;
import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBManager3 {

    private static final String GET_USER = "SELECT * FROM users u WHERE u.login = ?;";
    private static final String GET_TEAM = "SELECT * FROM teams t WHERE t.name = ?;";
    private static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES";
    private static final String GET_USER_TEAMS = "SELECT teams.* FROM teams " +
            "JOIN users_teams ON users_teams.team_id = teams.id " +
            "WHERE users_teams.user_id = ?";

    public User getUser(String login) throws DBException {
        try(Connection con = DBManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(GET_USER)) {
            pstmt.setString(1,login);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) return new User(rs.getInt(1), login);
            return null;
        } catch (SQLException e) {
            throw new DBException("Exception for getUser", e);
        }
    }
    public Team getTeam(String name) throws DBException {
        try(Connection con = DBManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(GET_TEAM)) {
            pstmt.setString(1,name);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) return new Team(rs.getInt(1), name);
            return null;
        } catch (SQLException e) {
            throw new DBException("Exception for getTeam", e);
        }
    }
    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        Statement pstmt = null;
        try {
            con = DBManager.getConnection();
            pstmt = con.createStatement();
            con.setAutoCommit(false);
            pstmt.executeUpdate(getSqlTeamsForUser(user, teams));
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new DBException("Exception for setTeamsForUser", e);
        } finally {
            closeAutoCloseable(pstmt);
            closeAutoCloseable(con);
        }
        return true;
    }

    private String getSqlTeamsForUser(User user, Team[] teams) {
        StringBuilder sql = new StringBuilder().append(SET_TEAMS_FOR_USER);
        for (Team team : teams) {
            sql.append("(").append(user.getId()).append(",").append(team.getId()).append("),");
        }
        int lastSeparator = sql.lastIndexOf(",");
        sql.replace(lastSeparator,lastSeparator+1,";");
        return sql.toString();
    }

    private void closeAutoCloseable(AutoCloseable con) {
        try {
            if (con != null) con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void rollback(Connection con) {
        try {
            con.rollback();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> result = new ArrayList<>();
        try(Connection con = DBManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(GET_USER_TEAMS)) {
            pstmt.setInt(1,user.getId());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                result.add(new Team(rs.getInt("id"), rs.getString("name")));
            }
        } catch (SQLException e) {
            throw new DBException("Exception for getUserTeams", e);
        }
        return result;
    }
}
