package com.epam.rd.java.basic.task5;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.DBManager;
import com.epam.rd.java.basic.task7.db.entity.Team;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBManager5 {

    private static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";

    public boolean updateTeam(Team team) throws DBException {
        try(Connection con = DBManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(UPDATE_TEAM)) {
            pstmt.setString(1, team.getName());
            pstmt.setInt(2, team.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Exception for updateTeam", e);
        }
        return true;
    }
}
