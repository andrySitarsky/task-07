package com.epam.rd.java.basic.task4;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.DBManager;
import com.epam.rd.java.basic.task7.db.entity.Team;

import java.sql.*;

public class DBManager4 {

    private static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";

    public boolean deleteTeam(Team team) throws DBException {
        try(Connection con = DBManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(DELETE_TEAM)) {
            pstmt.setInt(1, team.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Exception for deleteTeam", e);
        }
        return true;
    }
}
